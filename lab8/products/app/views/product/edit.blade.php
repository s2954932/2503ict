@extends('product.layout')

@section('heading')
     <div class="container-fluid">
        <h4>Edit product {{{ $product->name }}}</h4>
        <h4><a href="{{{ url('index') }}}">Home</a></h4>
    </div>
@stop

@section('content')
     {{ Form::model($product, array('method' => 'PUT', 'route' => array('product.update', $product->id))) }}    

        <!-- name -->
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name') }}
        {{ $errors->first('name') }}
        <br />
        {{ Form::label('price', 'Price') }}
        {{ Form::text('price') }}  
        {{ $errors->first('price') }}
        <br />
        {{ Form::submit('Update') }}

    {{ Form::close() }}
@stop