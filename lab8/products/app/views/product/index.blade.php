@extends('product.layout')

@section('heading')
 <div class="container-fluid">
    <h4>Product</h4>
</div>
@stop

@section('content')
    @foreach ( $products as $product )
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">product name: {{{ $product->name }}}</div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">${{{ $product->price }}}</div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><a href="product/{{{ $product->id }}}">View</a><br /><a href="product/{{{ $product->id }}}/edit">Edit</a><br />{{ Form::open(array('method' => 'DELETE', 'route' => array('product.destroy', $product->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}</div>
        </div>
    @endforeach
    <a href="product/create">Add new product</a>
@stop