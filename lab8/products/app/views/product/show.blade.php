@extends('product.layout')

@section('heading')
     <div class="container-fluid">
        <h4>Showing product {{{ $product->name }}}</h4>
        <h4><a href="{{{ url('index') }}}">Home</a></h4>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Product Name:</div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">{{{ $product->name }}}</div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Product Price:</div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">${{{ $product->price }}}</div>
    </div>
@stop