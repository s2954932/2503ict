@extends('product.layout')

@section('heading')
     <div class="container-fluid">
        <h4>Create new product</h4>
        <h4><a href="{{{ url('index') }}}">Home</a></h4>
    </div>
@stop

@section('content')
     {{ Form::open(array('action' => 'ProductController@store')) }}    

        {{ Form::label('name', 'Name') }}
        {{ Form::text('name') }}
        {{ $errors->first('name') }}
        <br />
        {{ Form::label('price', 'Price') }}
        {{ Form::text('price') }} 
        {{ $errors->first('price') }}
        <br />
        {{ Form::submit('Create Product') }}

    {{ Form::close() }}
@stop