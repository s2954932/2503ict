<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>My products</title>

	<!-- custom themes go here -->
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body style="background-color:rgba(0,0,0,0.1);">
    <nav class="navbar navbar-default">
    	@yield('heading')
    </nav>
	<div class="container">
	    <div class="row">
	        <div class="col-lg-3 col-md-3 col-sm-2"></div>
	        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" style="background-color:rgb(255,255,255);">
	        		 @section('content')
        	    
        	    	@show
	
    	    </div>
	        <div class="col-lg-3 col-md-3 col-sm-2"></div>
	    </div>
	</div>
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>