@extends('user.layout')

@section('heading')
 <div class="container-fluid">
    <h4>Login</h4>
</div>
@stop

@section('content')
   {{ Form::open(array('url' => secure_url('user/login'))) }}    

        {{ Form::label('username', 'User Name') }}
        {{ Form::text('username') }}
        <br />
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password') }} 
        <br />
        {{ Form::submit('submit') }}

    {{ Form::close() }}
@stop