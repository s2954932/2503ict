@extends('user.layout')

@section('heading')
 <div class="container-fluid">
    <h4>Add a user</h4>
</div>
@stop

@section('content')
   {{ Form::open(array('url' => secure_url('user/store'))) }}    

        {{ Form::label('username', 'User Name') }}
        {{ Form::text('username') }}
        {{ $errors->first('username') }}
        <br />
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password') }} 
        {{ $errors->first('password') }}
        <br />
        {{ Form::submit('Create') }}

    {{ Form::close() }}
@stop