<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function login()
	 {
	 	$user_data = array(
	 			'username' => Input::get('username'),
	 			'password' => Input::get('password')
	 		);
	 		
	 		
	 	if(Auth::attempt($user_data))
	 	{
	 		return Redirect::to(URL::previous());
	 	}else
	 	{
	 		Session::put('login_error', 'Login failed');
	 		return Redirect::to(URL::previous())->withInput();
	 	}
	 }
	 
	 public function logout()
	 {
	 	if(Auth::check())
	 	{
	 		Auth::logout();
	 	}
	 	
	 	return Redirect::action('ProductController@index');
	 }
	 
	public function index()
	{
		return View::make('user.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$results = Input::all();
		
		$v = Validator::make($results, User::$rules);
		 
		if($v->passes())
		{
			$user = new User;
			$user->username = $results['username'];
			$user->password = Hash::make($results['password']);
			$user->save();
			
			return View::make('product.index');
		}else
		{
			return Redirect::action('UserController@create')->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	function authenticate($username, $password) 
	{
		return Auth::attempt(array('username' => $username, 'password' => $password)); 
	} 
	
}
