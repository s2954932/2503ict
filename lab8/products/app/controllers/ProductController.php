<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		return View::make('product.index', compact('products'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::check())
		{
			return View::make('product.create');
		}else
		{
			return Redirect::to(URL::previous());
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Auth::check())
		{
		 $results = Input::all();
		 
		 $v = Validator::make($results, Product::$rules);
		 
		 if($v->passes())
		 {
			$product = new Product;
			$product->name = $results['name'];
			$product->price = $results['price'];
			$product->save();
			
			return View::make('product.show')->with('product', $product); 
		 }else{
		 	return Redirect::action('ProductController@create')->withErrors($v);
		 }
		}else
		{
			return Redirect::to(URL::previous());
		}
		 
	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$product = Product::find($id);
		return View::make('product.show')->with('product', $product); 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(Auth::check())
		{
		$product = Product::find($id);
		
		return View::make('product.edit')->with('product', $product);
		}else
		{
			return Redirect::to(URL::previous());
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(Auth::check())
		{
		 $results = Input::all();
		 
		 $v = Validator::make($results, Product::$rules);
		 
		 if($v->passes())
		 {
		 	$product = Product::find($id);
			$product->name = $results['name'];
			$product->price = $results['price'];
			$product->save();
			 
			$product = Product::find($id);
			 
			return View::make('product.show')->with('product', $product); 
		 }else{
		 	return Redirect::action('ProductController@edit', $id)->withErrors($v);
		 }
		}else
		{
			return Redirect::to(URL::previous());
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Auth::check())
		{
		$product = Product::find($id);
		
		$product->delete();
		return Redirect::route('product.index');
		}else
		{
			return Redirect::route('product.index');
		}
	}


}
