<?php
    class ProductSeeder extends Seeder {
        public function run()
        {
            $product = new Product;
            $product->name = 'apple';
            $product->price = 2.55;
            $product->save();
            
            $product = new Product;
            $product->name = 'orange';
            $product->price = 1.25;
            $product->save();
        }
    }