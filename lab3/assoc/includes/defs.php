<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form where $name = name OR state OR year **task 6** */
function search($name) {
    global $pms; 

    // Filter $pms by $name
    if (!empty($name)) {
	$results = array();
	foreach ($pms as $pm) {	//modified to search through name, from, to and state with the one input **task 6**
	    if (stripos($pm['name'], $name) !== FALSE || 
	    	strpos($pm['from'], $name) !== FALSE || 
	        strpos($pm['to'], $name) !== FALSE ||
	        stripos($pm['state'], $name) !== FALSE) {
		$results[] = $pm;
	    }
	}
	$pms = $results;
    }
    
    return $pms;
}
?>
