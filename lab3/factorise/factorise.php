<?php
/*
 * Script to print the prime factors of a single positive integer
 * sent from a form.
 * BAD STYLE: Does not use templates.
 */
include "includes/defs.php";

# Set $number to the value entered in the form.
$number = $_GET['number'];
$errorMsg;    //to hold the error message instead of exiting page.. **task 5**

# Check $number is nonempty, numeric and between 2 and PHP_MAX_INT = 2^31-1.
# (PHP makes it difficult to do this naturally; see the manual.)
if (empty($number)) { //in all statements replaced echo with filling errorMsg variable and removed the 'exit'.. **task 5**
    $errorMsg = "Error: Missing value\n";
} else if (!is_numeric($number)) {
    $errorMsg = "Error: Nonnumeric value: $number\n";
} else if ($number < 2 || $number != strval(intval($number))) {
    $errorMsg = "Error: Invalid number: $number\n";
} else {
    # Set $factors to the array of factors of $number.
    $factors = factors($number);
    # Set $factors to a single dot-separated string of numbers in the array.
    $factors = join(" . ", $factors);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Factors</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="styles/wp.css">
  </head>
  
  <body>  
    <h1>Factorisation</h1>

    <p>
      <?php 
        if(isset($errorMsg)){ //has the user entered an input to trigger an error?
          echo $errorMsg; //if so echo the error message
          
           $file = fopen('tf.txt', 'a'); //file to append factorials to in order to store.. ** extention **
          
           fwrite($file, $errorMsg);
           fflush($file);
        
           fclose($file);
        }else{
          echo "$number = $factors";  //otherwise operate as per usual **task 5**
          
          $file = fopen('tf.txt', 'a'); //file to append factorials to in order to store.. ** extention **
          
          fwrite($file, "$number = $factors \n");
          fflush($file);
        
          fclose($file);
        }
      ?>
    </p>

    <form method="get" action="factorise.php">
      <p>Number to factorise: <input type="text" value="<?php echo $number; ?>" name="number">
      <p><input type="submit" value="Factorise it!">
    </form>
    <hr>
    <table>
      <tr>
        <td>
          <p>
            Sources: <br />
            <a href="show.php?file=factorise.php">factorise.php</a><br />
            <a href="show.php?file=includes/defs.php">includes/defs.php</a>
          </p>
        </td>
        <td>
          <p>
            Log: <br />
            <a href="showFile.php">display log</a>
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
