<?php 
	session_start();
	
	//random number 1-10 to display amount of messages
	$randNum = rand(1,10);
	
	$_SESSION['randNum'] = $randNum;
	
	if(isset($_SESSION['loggedIn'])){
	}else{
		$_SESSION['loggedIn'] = false;
	}
	$messageArr = array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>My Social media</title>

	<!-- custom themes go here -->
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<link type="text/css" rel="stylesheet" href="style.css" />
</head>
<body>
	<div class="container">
	<?php
		//is the user logged in?
		//if($_SESSION['loggedIn']){
	?>
	
	<!--NAVIGATION-->
	<nav class="navbar-fixed-top">
		  <div class="row dr_b">
				<div class="col-md-4 col-sm-4 col-xs-4"><h4>Matts Social Media</h4></div>
				<div class="col-md-5 col-sm-5 col-xs-2"></div>
				<div class="col-md-1  col-sm-1 col-xs-2"><a href="#">Photos</a></div>
				<div class="col-md-1  col-sm-1 col-xs-2"><a href="#">Friends</a></div>
				<div class="col-md-1  col-sm-1 col-xs-2"><?php if($_SESSION['loggedIn']){echo '<a href="#">Logout</a>';}else{echo 'Login';}//{header(index.php);} ?></div>
		  </div>
	  </nav>
	  <!--END OF NAVIGATION-->
	<div id="page">
	  <div class="row">
			<!--LEFT HALF OF PAGE-->
			<div class="col-md-4">
				<div class="row" style="padding-bottom: 20px;">
					<div class="col-md-4">Name:</div>
					<div class="col-md-8">Matthew Owen-Jones</div>
				 </div>
				 <div class="row">
					<div class="col-md-12"></div>
				 </div>
				 <div class="row">
					<div class="col-md-11"><form style="border:	2px solid;" action="postmessage.php" method="post" enctype="multipart/form-data">Message: <br /><textarea rows="4" cols="35" name="message"></textarea><br />
						Insert an image: <br /><input type="file" name="picture" id="picture"><br /><input type="submit" value="Submit" /></form>
					</div>
					<div class="col-md-1"></div>
				 </div>
				 <div class="row">
					<div class="col-md-12"><?php echo 'generating ' . $_SESSION['randNum'] . ' messages:'; ?></div>
				 </div>
			</div>
			
			<!--RIGHT HALF OF PAGE-->
			<div class="col-md-8">
				 <?php
					//sql get all messages stored in db.. coming soon
					
					//messages stored in array for the moment
						$messageArr[0] = array('<img src="css/1.jpg" alt="image1" />',date("d,m,Y"), date("h:i:sa"),"message one");
						$messageArr[1] = array('<img src="css/2.jpg" alt="image2" />',date("d,m,Y"), date("h:i:sa"),"message two");
						$messageArr[2] = array('<img src="css/3.jpg" alt="image3" />',date("d,m,Y"), date("h:i:sa"),"message three");
						$messageArr[3] = array('<img src="css/4.jpg" alt="image4" />',date("d,m,Y"), date("h:i:sa"),"message four");
						$messageArr[4] = array('<img src="css/5.jpg" alt="image5" />',date("d,m,Y"), date("h:i:sa"),"message five");
						$messageArr[5] = array('<img src="css/6.jpg" alt="image6" />',date("d,m,Y"), date("h:i:sa"),"message six");
						$messageArr[6] = array('<img src="css/7.jpg" alt="image7" />',date("d,m,Y"), date("h:i:sa"),"message seven");
						$messageArr[7] = array('<img src="css/8.jpg" alt="image8" />',date("d,m,Y"), date("h:i:sa"),"message eight");
						$messageArr[8] = array('<img src="css/9.jpg" alt="image9" />',date("d,m,Y"), date("h:i:sa"),"message nine");
						$messageArr[9] = array('<img src="css/10.jpg" alt="image10" />',date("d,m,Y"), date("h:i:sa"),"message ten");
					
					//display user entered message(if submitted)
					$file = fopen("posts.txt", "r");
					if(filesize("posts.txt") > 0){
						while(! feof($file)){
							$line = fgets($file);
							$arr = explode(" ", $line);		//explode $line string into an array
							
							if(count($arr) > 1){
								$counta = 0;		//length of image string
								$countb = 0;		//length of date string
								$countc = 0;		//length of time string
								$countd = count($arr);	//length of message string
								$imgArr;			//image array
								$dteArr;			//date array
								$tmeArr;			//time array
								$msgArr;			//message array
								$imgStr = "";		//image string
								$dteStr = "";		//date string
								$tmeStr = "";		//time string
								$msgStr = "";		//message string
								
								for($i = 0; $i < count($arr); $i++){		//sort through the line array and populate the length of each array
									if(strcmp($arr[$i], '././') == 0){
										if($counta == 0){	
											$counta = $i;					//populate length of image array
										}else if($countb == 0){
											$countb = $i;					//populate length of date array
										}else if($countc == 0){
											$countc = $i;					//populate length of time array
										}
									}
								}
								$imgArr = array_slice($arr,0,$counta);			//splits line array to length of a image array
								$dteArr = array_slice($arr,0,$countb);			//splits line array to length of a date array
								$tmeArr = array_slice($arr,0,$countc);			//splits line array to length of a time array
								$msgArr = array_slice($arr,0,$countd);			//splits line array to length of a message array
								
								for($i = 0; $i < count($imgArr); $i++){		//populates image string
									if($i == 0){
										$imgStr = $imgArr[$i];
									}else{
										$imgStr = $imgStr . ' ' . $imgArr[$i];
									}
								}
								
								for($i = $counta+1; $i < count($dteArr); $i++){		//populates date string
									$dteStr = $dteStr . ' ' . $dteArr[$i];
								}
								
								for($i = $countb+1; $i < count($tmeArr); $i++){		//populates time string
									$tmeStr = $tmeStr . ' ' . $tmeArr[$i];
								}
								
								for($i = $countc+1; $i < count($msgArr)-1; $i++){	//populates message string
									$msgStr = $msgStr . ' ' . $msgArr[$i];
								}
								
								
								echo '<div class="row" style="padding-top: 5px;padding-bottom: 5px;border-right: 2px solid;border-left: 2px solid;border-top: 2px solid;border-bottom: 2px solid;">
									<div class="col-md-4 col-sm-6">'. $imgStr .'</div>
									<div class="col-md-2"></div>
									<div class="col-md-6 col-sm-6">'. $dteStr .'   '. $tmeStr .'<br />'. $msgStr .'</div>
								</div>';
							}
						}
					}
					fclose($file);
						
					//destruct 2d array
					for($i = 0; $i < $_SESSION['randNum']; $i++){
						$img = $messageArr[$i][0];	//image
						$dte = $messageArr[$i][1];	//date
						$tme = $messageArr[$i][2];	//time
						$msg = $messageArr[$i][3];	//message
						
						//display messages
						echo '<div class="row" style="padding-top: 5px;padding-bottom: 5px;border-right: 2px solid;border-left: 2px solid;border-top: 2px solid;border-bottom: 2px solid;">
							<div class="col-md-4 col-sm-6">'. $img .'</div>
							<div class="col-md-2"></div>
							<div class="col-md-6 col-sm-6">'. $dte .' '. $tme .'<br />'. $msg .'</div>
						</div>';
					}
				  ?>
			</div>
	  </div>
	 </div>

	 <!--
	 <?php
		//display login page.. user isn't logged in.
		//}else{
	 ?>
		<div class="row dr_b">
			<div class="col-md-4 col-sm-4 col-xs-4"><h4>Matts Social Media</h4></div>
			<div class="col-md-5 col-sm-5 col-xs-2"></div>
			<div class="col-md-1  col-sm-1 col-xs-2"></div>
			<div class="col-md-1  col-sm-1 col-xs-2"></div>
			<div class="col-md-1  col-sm-1 col-xs-2"></div>
		</div> 
		<div id="logincss">
			<h1>Login</h1>
			<form action="login.php" method="post">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12"><input type="text" name="username" required placeholder="User Name (email address)" class="form-control" id="inputError1"/></div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12"><input type="password" name="password" required placeholder="Password" class="form-control" id="inputError1"/></div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6"><input class="fullsize" type="submit" value="Log In" /></div>
					<div class="col-md-6 col-sm-6 col-xs-6"><button class="btn-block"><a href="#">Register</a></button></div>
				</div>
			</form>
		</div>
	 <?php
		//}
	 ?>
	 -->
	</div>
</body>
</html>