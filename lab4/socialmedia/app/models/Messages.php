<?php
    function fillmsgs()
    {
        $messageArr = array(
            array('image' => '<img src="css/1.jpg" alt="image1" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message one"),
            array('image' => '<img src="css/2.jpg" alt="image2" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message two"),
    		array('image' => '<img src="css/3.jpg" alt="image3" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message three"),
    		array('image' => '<img src="css/4.jpg" alt="image4" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message four"),
    		array('image' => '<img src="css/5.jpg" alt="image5" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message five"),
    		array('image' => '<img src="css/6.jpg" alt="image6" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message six"),
    		array('image' => '<img src="css/7.jpg" alt="image7" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message seven"),
    		array('image' => '<img src="css/8.jpg" alt="image8" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message eight"),
    		array('image' => '<img src="css/9.jpg" alt="image9" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message nine"),
    		array('image' => '<img src="css/10.jpg" alt="image10" />', 'dte' => date("d,m,Y"), 'tme' => date("h:i:sa"), 'message' => "message ten")
		);
		
		return $messageArr;
    }
?>