<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

require "models/Messages.php";

Route::get('/', function()
{
  $posts = fill();
  
  $randNum = rand(1,10);
  
	return View::make('socialmedia.social')
	              -> with('posts', $posts)
	              -> with('randNum', $randNum);
});

Route::post('postmessage', function()
{
  return View::make('socialmedia.posted');
});



function fill() {
  $messageArr = fillmsgs();
  
    $results = array();
    
    foreach ($messageArr as $msg) {
      $results[] = $msg;
    }
    $pms = $results;
    
    return $pms;
}

