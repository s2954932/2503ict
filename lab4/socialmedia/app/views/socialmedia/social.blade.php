@extends('layouts.master')

@section('pageleft')
	<div class="row" style="padding-bottom: 20px;">
		<div class="col-md-4">Name:</div>
		<div class="col-md-8">Matthew Owen-Jones</div>
	 </div>
	 <div class="row">
		<div class="col-md-12"></div>
	 </div>
	 <div class="row">
		<div class="col-md-11"><form style="border:	2px solid;" action="postmessage" method="post" enctype="multipart/form-data">Message: <br /><textarea rows="4" cols="35" name="message"></textarea><br />
			Insert an image: <br /><input type="file" name="picture" id="picture"><br /><input type="submit" value="Submit" /></form>
		</div>
		<div class="col-md-1"></div>
	 </div>
	 <div class="row">
		<div class="col-md-12">generating {{{ $randNum }}} messages</div>
	 </div>
	</div>
@stop

@section('pageright')
<?php
	//destruct 2d array
	for($i = 0; $i < $randNum; $i++){
		$line = $posts[$i];
		
		//display messages
		echo '<div class="row" style="padding-top: 5px;padding-bottom: 5px;border-right: 2px solid;border-left: 2px solid;border-top: 2px solid;border-bottom: 2px solid;">
			<div class="col-md-4 col-sm-6">'. $line["image"] .'</div>
			<div class="col-md-2"></div>
			<div class="col-md-6 col-sm-6">'. $line["dte"] .' '. $line["tme"] .'<br />'. $line["message"] .'</div>
		</div>';
	}
?>

@stop