<?php
    if(isset($_SESSION['loggedIn'])){
        
    }else{
        $_SESSION['loggedIn'] = false;
    }
    
    $_SESSION['random'] = rand(1, 10);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>My Social media</title>

	<!-- custom themes go here -->
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<link type="text/css" rel="stylesheet" href="style.css" />
</head>
<body>
	<div class="container">
	
	<!--NAVIGATION-->
	<nav class="navbar-fixed-top">
		  <div class="row dr_b">
				<div class="col-md-4 col-sm-4 col-xs-4"><h4>Matts Social Media</h4></div>
				<div class="col-md-5 col-sm-5 col-xs-2"></div>
				<div class="col-md-1  col-sm-1 col-xs-2"><a href="#">Photos</a></div>
				<div class="col-md-1  col-sm-1 col-xs-2"><a href="#">Friends</a></div>
				<div class="col-md-1  col-sm-1 col-xs-2"><?php if($_SESSION['loggedIn']){echo '<a href="#">Logout</a>';}else{echo 'Login';}//{header(index.php);} ?></div>
		  </div>
	  </nav>
	  <!--END OF NAVIGATION-->
	<div id="page">
	  <div class="row">
			<!--LEFT HALF OF PAGE-->
			<div class="col-lg-4 col-md-4 col-sm-8">
			    @section('pageleft');
			</div>
			 @show()
			
			<!--RIGHT HALF OF PAGE-->
			<div class="col-lg-8 col-md-8 col-sm-8">
				 @section('pageright')
			</div>
			 @show()
	  </div>
	 </div>
	</div>
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>